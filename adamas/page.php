<?php
/**
 * @package WordPress
 * @subpackage Adamas
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'hero_section' ): 
            get_template_part( 'template-parts/page/hero_section' );
        elseif( get_row_layout() == 'about_us_section' ): 
            get_template_part( 'template-parts/page/about_us_section' );
        elseif( get_row_layout() == 'services_section' ): 
            get_template_part( 'template-parts/page/services_section' );
        elseif( get_row_layout() == 'news_section' ): 
            get_template_part( 'template-parts/page/news_section' );
        elseif( get_row_layout() == 'clients_section' ): 
            get_template_part( 'template-parts/page/clients_section' );
        elseif( get_row_layout() == 'packaging_section' ): 
            get_template_part( 'template-parts/page/packaging_section' );
        elseif( get_row_layout() == 'b2b_section' ): 
            get_template_part( 'template-parts/page/b2b_section' );
        elseif( get_row_layout() == 'appointment_section' ): 
            get_template_part( 'template-parts/page/appointment_section' );
        elseif( get_row_layout() == 'offices_section' ): 
            get_template_part( 'template-parts/page/offices_section' );
        elseif( get_row_layout() == 'contacts_text' ): 
            get_template_part( 'template-parts/page/contacts_text' );
        endif;
    endwhile;
endif;

get_footer();