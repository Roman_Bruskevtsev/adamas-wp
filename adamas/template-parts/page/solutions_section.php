<section class="adm-solutions__section scroll__section" id="solutions">
	<?php if( get_sub_field('image') ) { ?>
	<div class="container-fluid no__padding">
		<div class="row">
			<div class="col">
				<div class="image" style="background-image: url(<?php the_sub_field('image'); ?>);"></div>
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="container">
		<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col-lg-9">
				<div class="content" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<div class="adm-section__title dark">
						<?php if( get_sub_field('small_title') ) { ?>
							<h4><?php the_sub_field('small_title'); ?></h4>
						<?php }
						if( get_sub_field('title') ) { ?>
							<h2><?php the_sub_field('title'); ?></h2>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php } 
		$solutions = get_sub_field('solutions'); 
		if( $solutions ) { ?>
		<div class="row">
			<div class="col">
				<div class="d-none d-lg-block">
					<div class="row">
						<?php 
						$delay = 200;
						foreach ( $solutions as $solution ) { ?>
						<div class="col-lg-6">
							<div class="adm-solution__block" data-aos="fade-up" data-aos-delay="<?php echo $delay; ?>">
								<?php if( $solution['icon'] ) { ?>
								<div class="icon">
									<img src="<?php echo $solution['icon']['url']; ?>" alt="<?php echo $solution['icon']['title']; ?>">
								</div>
								<?php }
								if( $solution['title'] || $solution['text'] ) { ?>
								<div class="text">
									<?php if( $solution['title'] ) { ?>
										<h3><?php echo $solution['title']; ?></h3>
									<?php } 
									echo $solution['text']; ?>
								</div>
								<?php } ?>
							</div>
						</div>
						<?php $delay = $delay + 100; } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row d-block d-lg-none">
			<div class="col">
				<div class="adm-solutions__slider swiper-container">
					<div class="swiper-wrapper">
					<?php foreach ( $solutions as $solution ) { ?>
						<div class="swiper-slide">
							<div class="adm-solution__slide text-center">
								<?php if( $solution['icon'] ) { ?>
								<div class="icon">
									<img src="<?php echo $solution['icon']['url']; ?>" alt="<?php echo $solution['icon']['title']; ?>">
								</div>
								<?php }
								if( $solution['title'] || $solution['text'] ) { ?>
								<div class="text">
									<?php if( $solution['title'] ) { ?>
										<h2><?php echo $solution['title']; ?></h2>
									<?php } 
									echo $solution['text']; ?>
								</div>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
					</div>
					<div class="swiper-pagination"></div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>