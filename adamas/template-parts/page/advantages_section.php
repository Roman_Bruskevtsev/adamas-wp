<section class="adm-advantages__section scroll__section" id="our-advantages">
	<div class="container">
		<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="adm-section__title grey" data-aos="fade-up">
					<?php if( get_sub_field('small_title') ) { ?>
						<h4><?php the_sub_field('small_title'); ?></h4>
					<?php }
					if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } 
		$advantages = get_sub_field('advantages');
		if( $advantages ) { ?>
		<div class="row">
		<?php 
		$delay = 200;
		foreach ( $advantages as $advantage ) { 
			$image = $advantage['image'] ? ' style="background-image: url('.$advantage['image'].')"' : ''; ?>
			<div class="col-sm-6 col-lg-3">
				<div class="adm-advantage__block" data-aos="fade-up" data-aos-delay="<?php echo $delay; ?>">
					<div class="image"<?php echo $image; ?>></div>
					<div class="content">
						<?php if( $advantage['title'] ) { ?>
							<h3><?php echo $advantage['title']; ?></h3>
						<?php } 
						echo $advantage['text']; ?>
					</div>
				</div>
			</div>
		<?php $delay = $delay + 100; } ?>
		</div>
		<?php } ?>
	</div>
</section>