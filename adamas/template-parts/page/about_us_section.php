<section class="adm-about__us__section scroll__section" id="about-us">
	<?php if( get_sub_field('image') ) { ?>
	<div class="image" style="background-image: url('<?php the_sub_field('image'); ?>');"></div>
	<?php } ?>
	<div class="container">
		<div class="row">
			<div class="col-lg-6"></div>
			<div class="col-lg-6">
				<div class="content" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
					<div class="row">
						<div class="col">
							<div class="adm-section__title green">
								<?php if( get_sub_field('small_title') ) { ?>
									<h4><?php the_sub_field('small_title'); ?></h4>
								<?php }
								if( get_sub_field('title') ) { ?>
									<h2><?php the_sub_field('title'); ?></h2>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php } 
					if( get_sub_field('text_1') || get_sub_field('text_2') ) { ?>
					<div class="row">
						<div class="col-lg-6">
							<?php if( get_sub_field('text_1') ) { ?>
								<div class="text"><?php the_sub_field('text_1'); ?></div>
							<?php } ?>
						</div>
						<div class="col-lg-6">
							<?php if( get_sub_field('text_2') ) { ?>
								<div class="text"><?php the_sub_field('text_2'); ?></div>
							<?php } ?>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>