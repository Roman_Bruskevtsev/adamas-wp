<section class="adm-services__section scroll__section" id="services">
	<div class="image"></div>
	<div class="container">
		<?php if( get_sub_field('small_title') || get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<div class="adm-section__title grey" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
					<?php if( get_sub_field('small_title') ) { ?>
						<h4><?php the_sub_field('small_title'); ?></h4>
					<?php }
					if( get_sub_field('title') ) { ?>
						<h2><?php the_sub_field('title'); ?></h2>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } 
		$services = get_sub_field('services');
		if( $services ) { ?>
		<div class="row">
			<div class="col">
				<div class="adm-services__row">
					<div class="row">
					<?php
					$delay = 200; 
					foreach( $services as $service ) { ?>
						<div class="col-lg-4">
							<div class="adm-service__block" data-aos="fade-right" data-aos-delay="<?php echo $delay; ?>" data-aos-duration="500">
							<?php if( $service['icon'] ) { ?>
								<div class="icon">
									<img src="<?php echo $service['icon']['url']; ?>" alt="<?php echo $service['icon']['title']; ?>">
								</div>
							<?php }
							if( $service['title'] ) { ?>
								<h3><?php echo $service['title']; ?></h3>
							<?php } 
							echo $service['text']; ?>
							</div>
						</div>
					<?php $delay = $delay + 200; } ?>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>