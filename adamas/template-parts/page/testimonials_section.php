<?php
$background = get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background').')"' : '';
?>
<section class="adm-testimonials__section scroll__section"<?php echo $background; ?> id="testimonials">
	<?php if( get_sub_field('slider') ) { ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 position-relative">
				<div class="adm-testimonials__slider swiper-container" data-aos="fade-up">
					<div class="swiper-wrapper">
						<?php foreach ( get_sub_field('slider') as $slide ) { ?>
						<div class="swiper-slide">
							<div class="content">
								<?php echo $slide['text']; 
								if( $slide['author'] ) { ?>
									<h4><?php echo $slide['author']; ?></h4>
								<?php } ?>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="swiper-button-prev"></div>
				<div class="swiper-button-next"></div>
			</div>
		</div>
	</div>
	<?php } ?>
</section>