<?php 
$background = get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background').')"' : '';
?>
<section class="adm-hero__section scroll__section"<?php echo $background; ?> id="hero">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="content__wrapper">
					<div class="content">
					<?php if( get_sub_field('small_title') || get_sub_field('title') || get_sub_field('button_url') && get_sub_field('button_label') ) { ?>
						<div class="adm-section__title" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500">
							<?php if( get_sub_field('small_title') ) { ?>
								<h4><?php the_sub_field('small_title'); ?></h4>
							<?php }
							if( get_sub_field('title') ) { ?>
								<h1><?php the_sub_field('title'); ?></h1>
							<?php }
							if( get_sub_field('title') ) { ?>
							<div class="text">
								<?php the_sub_field('text'); ?>
							</div>
							<?php }
							if( get_sub_field('button_url') && get_sub_field('button_label') ) { ?>
								<a class="btn btn__primary" href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_label'); ?></a>
							<?php } ?>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>