<?php
/**
 * @package WordPress
 * @subpackage Adamas
 * @since 1.0
 * @version 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header class="adm-header">
        <div class="container">
            <div class="row">
                <div class="col-4 col-lg-2">
                    <?php if( get_field('logo', 'option') ) { ?>
                    <a class="adm-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo get_field('logo', 'option')['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                </div>
                <div class="col-8 col-lg-10">
                    <?php if( has_nav_menu('main') ) { ?>
                    <div class="adm-mobile__btn float-end d-block d-lg-none">
                        <span></span>
                        <span></span>
                    </div>
                    <div class="adm-main__menu float-end d-none d-lg-block">
                        <?php wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'adm-main__nav'
                        ) ); ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </header>
    <?php if( has_nav_menu('main') ) { ?>
    <div class="adm-mobile__menu d-block d-lg-none">
        <?php wp_nav_menu( array(
            'theme_location'        => 'main',
            'container'             => 'nav',
            'container_class'       => 'adm-mobile__nav'
        ) ); ?>
    </div>
    <?php } ?>
    <main>