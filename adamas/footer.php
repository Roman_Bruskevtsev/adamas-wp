<?php
/**
 * @package WordPress
 * @subpackage Adamas
 * @since 1.0
 * @version 1.0
 */
$footer = get_field('footer', 'option'); ?>  
    </main>
    <footer class="adm-footer scroll__section" id="contacts">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="adm-footer__contacts" data-aos="fade-up">
                        <?php if( $footer['title'] ) { ?>
                        <div class="row">
                            <div class="col">
                                <div class="adm-footer__title">
                                    <h2><?php echo $footer['title']; ?></h2>
                                </div>
                            </div>
                        </div>
                        <?php } 
                        if( $footer['location'] || $footer['email'] ) { ?>
                        <div class="row">
                            <?php if( $footer['location'] ) { ?>
                            <div class="col-lg-6">
                                <div class="adm-footer__contact">
                                    <h6><?php _e('Location'); ?></h6>
                                    <p><?php echo $footer['location']; ?></p>
                                </div>
                            </div>
                            <?php }
                            if( $footer['email'] ) { ?>
                            <div class="col-lg-6">
                                <div class="adm-footer__contact">
                                    <h6><?php _e('Email'); ?></h6>
                                    <a href="mailto:<?php echo $footer['email']; ?>"><?php echo $footer['email']; ?></a>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php } 
                        if( $footer['phone'] ) { ?>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6">
                                <div class="adm-footer__contact phone">
                                    <h6><?php _e('Contact number'); ?></h6>
                                    <a href="tel:<?php echo $footer['phone']; ?>"><?php echo $footer['phone']; ?></a>
                                </div>
                            </div>
                        </div>
                        <?php } 
                        if( $footer['logo'] || $footer['copyrights'] ) { ?>
                        <div class="row">
                            <?php if( $footer['logo'] ) { ?>
                            <div class="col-lg-6">
                                <a class="adm-footer__logo d-none d-lg-block" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                    <img src="<?php echo $footer['logo']['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                                </a>
                            </div>
                            <?php }
                            if( $footer['copyrights'] ) { ?>
                            <div class="col-lg-6">
                                <div class="adm-footer__copyrights d-none d-lg-block">
                                    <h6><?php echo $footer['copyrights']; ?></h6>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php if( $footer['form_title'] || $footer['form_shortcode'] ) { ?>
                <div class="col-lg-6">
                    <div class="adm-footer__form" data-aos="fade-up">
                    <?php if( $footer['form_title'] ) { ?>
                        <div class="title">
                            <h3><?php echo $footer['form_title']; ?></h3>
                        </div>
                    <?php } 
                    if( $footer['form_shortcode'] ) { ?>
                        <div class="form"><?php echo do_shortcode( $footer['form_shortcode'] ); ?></div>
                    <?php } ?>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="row">
                <?php if( $footer['logo'] ) { ?>
                <div class="col-12">
                    <a class="adm-footer__logo text-center d-block d-lg-none" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo $footer['logo']['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                </div>
                <?php }
                if( $footer['copyrights'] ) { ?>
                <div class="col-12">
                    <div class="adm-footer__copyrights text-center d-block d-lg-none">
                        <h6><?php echo $footer['copyrights']; ?></h6>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>