<?php
/**
 * @package WordPress
 * @subpackage Adamas
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'hero_section' ):
            get_template_part( 'template-parts/page/hero_section' );
        elseif( get_row_layout() == 'about_us_section' ): 
            get_template_part( 'template-parts/page/about_us_section' );
        elseif( get_row_layout() == 'services_section' ): 
            get_template_part( 'template-parts/page/services_section' );
        elseif( get_row_layout() == 'solutions_section' ): 
            get_template_part( 'template-parts/page/solutions_section' );
        elseif( get_row_layout() == 'advantages_section' ): 
            get_template_part( 'template-parts/page/advantages_section' );
        elseif( get_row_layout() == 'testimonials_section' ): 
            get_template_part( 'template-parts/page/testimonials_section' );
        endif;
    endwhile;
endif;

get_footer();